console.log('Hii this is my website');
let source = 'bbc-news';
let apiKey = 'bd39805fa5eb44619f932650642347a4';

//Grab the News container
let newsAccordion = document.getElementById('newsAccordion');
//Create an ajax get request
const xhr = new XMLHttpRequest();
xhr.open('GET', `https://newsapi.org/v2/top-headlines?country=us&category=science&apiKey=${apiKey}`,`https://newsapi.org/v2/top-headlines?country=in&category=science&apiKey=${apiKey}`, true);

// What to do when response is ready
xhr.onload = function () {
    if (this.status === 200) {
        let json = JSON.parse(this.responseText);
        let articles = json.articles;
        console.log(articles);
        let newsHtml = "";
        articles.forEach(function(element, index) {
            // console.log(element, index)
            let news = ` <main class="main columns">
            <section class="column main-column">
               
                <div class="article-body">
                  <h2 class="article-title">
                  <a href="${element['url']}">${element['title']} </a> </h2>
                  <p  style="padding:10px; margin-left:-10px;"  class="article-content">
                  ${element['content']}
                    </p>
                    <div class="set" style="  display: block;
                    margin-left: 50px;
                    margin-right: 50px; padding:30px;">
                  <footer class="article-info" style="width:100%; ">
                  ${element['description']}
                   </footer>
                <figure class="article-image is-4by3">
                <img src="${element['urlToImage']}" alt="" style="width:100%; height:300px; ">
              </figure>
              </div>
          </div>
          <hr style="  border: 2px dotted black;
          border-radius: 50%; width:100%; margin-top:20px; margin-bottom:50px">
   
  
             </section>
          
          </main>
          
          
          
               `;
            newsHtml += news;
        });
        newsAccordion.innerHTML = newsHtml;
    }
    else {
        console.log("Some error occured")
    }
}

xhr.send()


