console.log('Hii this is my website');
let source = 'bbc-news';
let apiKey = 'bd39805fa5eb44619f932650642347a4';

//Grab the News container
let newsAccordion = document.getElementById('newsAccordion');
//Create an ajax get request
const xhr = new XMLHttpRequest();
xhr.open('GET', `https://newsapi.org/v2/top-headlines?country=us&category=health&apiKey=${apiKey}`, true);

// What to do when response is ready
xhr.onload = function () {
    if (this.status === 200) {
        let json = JSON.parse(this.responseText);
        let articles = json.articles;
        console.log(articles);
        let newsHtml = "";
        articles.forEach(function(element, index) {
            // console.log(element, index)
            let news = `        <section class = "banner">
            <div class = "banner-sub-content">
                <div class = "hot-topic">

                    <div class = "hot-topic-content" style="padding:5px;">
                        <h2>${element['title']}
                        </h2>
                        
                        <p style="padding:15px; margin-left:-16px;">${element['description']} 
                        </p>
                        <marquee  class="marq1">${element['description']} 
                        </marquee>
                        <a  href = "${element['url']} ">Read More</a>
                    </div>
                    
                    <img src = "${element['urlToImage']}" alt = "" style="  display: block;
                    margin-left: auto;
                    margin-right: auto;
                    width: 60%; height:300px; padding:20px;">
                    <hr style="  border: 2px dotted black;
        border-radius: 50%; width:100%; margin-top:20px; margin-bottom:50px">
 
                </div>
                
                </div>
                </section>`;
            newsHtml += news;
        });
        newsAccordion.innerHTML = newsHtml;
    }
    else {
        console.log("Some error occured")
    }
}

xhr.send()


