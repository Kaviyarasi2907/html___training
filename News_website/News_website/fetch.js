console.log('Hii this is my website');
let source = 'bbc-news';
let apiKey = 'bd39805fa5eb44619f932650642347a4';

//Grab the News container
let newsAccordion = document.getElementById('newsAccordion');
//Create an ajax get request
const xhr = new XMLHttpRequest();
xhr.open('GET', `https://newsapi.org/v2/top-headlines?country=in&category=sports&apiKey=${apiKey}`, true);
// What to do when response is ready
xhr.onload = function () {
  
    if (this.status === 200) {
        let json = JSON.parse(this.responseText);
        let articles = json.articles;
        console.log(articles);
        let newsHtml = "";
        articles.forEach(function(element, index) {
            // console.log(element, index)
            let news = `<div class="card mb-3" style="max-width: 2000px;     background: linear-gradient(#3399FF,#D62AD0,#CD113B);
            color:white;">
            <div class="row g-0" style="padding-bottom:-10px">
              <div class="col-md-4">
                <img src="${element['urlToImage']}" class="img-fluid rounded-start" alt="...">
              </div>
              <div class="col-md-8">
                <div class="card-body">
                  <h5 class="card-title">${element['title']}</h5>
                  <p class="card-text">${element['description']}</p>
                  <p class="card-text">${element['publishedAt']}</small></p>
                  <button class="btn"><a href="${element['url']}" target="_blank" >Read more here</a>
                  </button>
                </div>
              </div>
            </div>
          </div>`;
            newsHtml += news;
        });
        newsAccordion.innerHTML = newsHtml;
    }
    else {
        console.log("Some error occured")
    }
}

xhr.send()


