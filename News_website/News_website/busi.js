console.log('Hii this is my website');
let source = 'bbc-news';
let apiKey = 'bd39805fa5eb44619f932650642347a4';

//Grab the News container
let newsAccordion = document.getElementById('newsAccordion');
//Create an ajax get request
const xhr = new XMLHttpRequest();
xhr.open('GET', `https://newsapi.org/v2/top-headlines?country=in&category=business&apiKey=${apiKey}`, true);

// What to do when response is ready
xhr.onload = function () {
    if (this.status === 200) {
        let json = JSON.parse(this.responseText);
        let articles = json.articles;
        console.log(articles);
        let newsHtml = "";
        articles.forEach(function(element, index) {
            // console.log(element, index)
            let news = `<div class="card text-center" style="    background: linear-gradient(#3399FF,#3B0000);
            color:white;">
            <div class="card-header">
            <a class="ele" href="${element['url']}">${element['title']}</a>
            </div>
            <div class="card-body" style="background-color:white;">
            <img src="${element['urlToImage']}" style="width:100%;"></img>

            </div>
            <div class="card-footer">
            <h5 style="font-size:18px;" class="card-title">${element['description']}</h5>

            </div>
          </div>
          
          
          
               `;
            newsHtml += news;
        });
        newsAccordion.innerHTML = newsHtml;
    }
    else {
        console.log("Some error occured")
    }
}

xhr.send()


